### 1: Install killport

```bash
$ curl -s https://gitlab.com/tdteam-public/shell/-/raw/master/killport.sh | sudo bash -E -
```
#### `Example`
```bash
$ killport 3000
```

### 2: Install server

#### ```1: Node```

```bash
$ curl -s https://gitlab.com/tdteam-public/shell/-/raw/master/server/node.sh | sudo bash -E -
```